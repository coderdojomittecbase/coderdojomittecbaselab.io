# CoderDojo-Mission-Python
author: bengoshi

Code and impress files for the unit mission-python at CoderDojo Berlin-Mitte @c-base.
This is produced for a present lesson and not for self-learners.

If there a any questions do not hesitate to ask.

For using the code:

* git clone https://gitlab.com/bengoshi/coderdojo-mission-python.git

* cd coderdojo-mission-python

* virtualenv -m python3 venv

* source venv/bin

* pip install pygame

* have fun

note for playing together: https://github.com/Ganapati/Simple-Game-Server

