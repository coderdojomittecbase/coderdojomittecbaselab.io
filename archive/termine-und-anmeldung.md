+++
title = "Termine und Anmeldung"

+++
Der na:chste Termine des CoderDojos ist:

* Samstag, 28. Dezember 2019 auf dem [Chaos Communication Congress](https://events.ccc.de/2019/10/04/36c3-in-leipzig/ "36C3")
Achtung - fu:r das Chaos Communication Congress brauchst Du unbedingt ein Ticket. Wenn Du noch keins hast, frage nochmal bengoshi. Aber an sich ist der Zug fu:r dieses Jahr abgefahren.
Insgesamt wurden 416 Tickets ausgegeben, davon 60 Tickets fu:r die Berliner Reisegruppe. :)

Alle Beteiligten der Reisegruppe sollten per E-Mail schon zahlreiche Infos bekommen haben. Falls Ihr noch nicht mit (langen) Info-Mails versorgt wurdet - bitte meldet Euch gleich per E-Mail bei bengoshi at c-base punkt org.

Wer seine E-Mail jetzt nicht raussuchen mo:chte, hier die wichtigsten Daten:

*Die „Reisegruppe“ reist unter Begleitung einiger Lehrer und Eltern. Einen herzlichen Dank an dieser Stelle den Betreuern!*

Wenn vor Ort was ist - fragt einen Engel nach einem DECT-Telefon und ruft durch. derMicha hat 8870, bengoshi 4847.

Der Reiseplan - wenn Ihr bis Leipzig Messe so bucht, braucht Ihr kein Nahverkehrsticket in Leipzig:

---

Hinfahrt:

7:35 Uhr Berlin Hauptbahnhof bis Bitterfeld, ICE 701, Treffpunkt Wagen
6, falls nicht ausreichend noch Wagen 5, Gleis 1. Leider habe ich noch keine Wagenreihung bekommen, um Euch den Buchstaben am Gleis zu sagen.

8:45 Uhr Bitterfeld -> 9:05 Uhr Leipzig Messe; S2 / RE nicht reservierbar.

---

Ru:ckfarht:

Treffpunkt 18:15 Uhr an „Fairydust“.

18:55 Uhr Leipzig Messe -> 19:15 Uhr Bitterfeld; S2 / RE nicht reservierbar

19:24 Uhr Bitterfeld -> 20:24 Uhr Berlin Hauptbahn, ICE 704, Wagen 1 für
diejenigen, die reservieren möchten.

---

Welche Veranstaltungen es aktuell speziell fu:r den Junghackertag gibt, seht Ihr hier:
[Junghacker-Veranstaltunen](https://events.ccc.de/congress/2019/wiki/index.php/Static:ForKids)
Im Kalender mu:sst Ihr auf den 28. klicken. Das ging leider nicht zu verlinken.

Da sind die „speziellen“ Veranstaltungen. Es gibt aber viele Workshops, die sind auch tauglich, aber haben Kinder und Jugendliche nicht ausdru:cklich als Zielgruppe. Da lohnt es sich zu sto:bern und zu schauen, was passen ko:nnte:
[SOS-Fahrplan](https://events.ccc.de/congress/2019/wiki/index.php/Static:Self-organized_Sessions)

Die Vortra:ge sind der „Kern“ des ganzen und findet in teilweise wirklich rießigen Vortragssa:len statt. Den Fahrplan hierzu findet Ihr hier:
[Fahrplan](https://fahrplan.events.ccc.de/congress/2019/Fahrplan/schedule/2.html)

Die Ero:ffnung des Junghackertages ist hier:
[Ero:ffnung](https://events.ccc.de/congress/2019/wiki/index.php/Session:Junghackertag_Er%C3%B6ffnung)

Menschen die sich dem weiblichen Geschlecht zugeho:rig fu:hlen, empfehlen wir besonders das Haecksenfru:hstu:ck. Da wa:re es scho:n, wenn Du etwas mitbringtst:
[Haecksenfru:hstu:ck](https://events.ccc.de/congress/2019/wiki/index.php/Session:Haecksenfr%C3%BChst%C3%BCck#_1790ca36cd1b19c22e12346d0a9c60d1)

* Reserviert die Workshops, die Ihr besuchen wollt. Infos gibt es jeweils bei den Workshops auf der Eventseite des CCC. Do it! Now!

* Denkt dran, Euch c3nav und 36c3 als App auf Eurem Smartphone zu installieren!

* Denkt an Euern Roller, wenn Ihr einen habt!

* Denkt an eine wiederauffu:llbare Flasche!

* „leeres“ T-Shirt, möglichst dunkel einpacken.

* falls Du ohne Eltern fa:hrst - hast Du beziehungsweise Deine Eltern schon die per E-Mail versandte Einversta:ndniserkla:rung abgegeben?

Und auch wenn es trivial klingt - es gibt den Begriff der Congressseuche. Nicht weil die Menschen dort unhygienisch wa:ren, sondern weil zur besten Erka:ltungszeit aus allen Teilen der Welt auf engem Raum viele Menschen zusammenkommen, feiern Bazillen und Viren dort Ihre Bescherung. Die wichtigsten Pra:ventionsmassnahmen: regelma:ssig Ha:nde waschen (mit Seife und mindestens zehn Sekunden) und viel trinken. Kein Essen anfassen, bevor die Ha:nde nicht gewaschen sind. Klingt trivial, hilft aber.

Mate ist cool, wird dort gefu:hlt von allen getrunken - und ist wirklich hochcoffeiniert. Haltet Euch bitte damit zuru:ck.

Ansonsten denkt bitte daran - keine Fotos, ausser alle im Bild auch wenn sie weit weg sind, sind ausdru:cklich damit einverstanden.



Wenn Ihr noch Fragen habt, meldet Euch einfach bei bengoshi.

**Und dann schon mal als Vormerker fu:r den Kalender: 18. Januar 2020 in der c-base, 22. Februar 2020 in der c-base und (noch in Planung) 14. oder 15. Ma:rz 2020 auf den Chemnitzer Linux-Tagen**

{{< figure src="/images/ATTiny85.jpg" title="ATTiny85" >}}

