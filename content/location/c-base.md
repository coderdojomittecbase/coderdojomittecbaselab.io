---
title: Berlins abgestu:rzte Raumstation

---
# c-base - Berlins abgestürtzte Raumstation

Was die c-base ist, la:sst sich nur bedingt beschreiben... am Ende wirst Du vorbeikommen müssen, um sie zu erleben. Viele denken, dass es irgendwas mit Computern zu tun hat. In Wirklichkeit ist eine Gemeinschaft von über 400 Mitgliedern, die gemeinsam die einst abgestu:rtze und unter Berlin liegende Raumstation ausgra:bt. Erste Artefakte wurden bereits 1995 gefunden. Die Vereinsmythologie begru:ndet das Aussehen der Raumstation mit angeschlossenem Hackerspace.
Bitte beachte, dass viele Mitglieder der Station keine Fotos oder Filmaufnahmen von sich wu:nschen. Wir wu:rden Dich daher bitten, auf solche zu verzichten oder vorher mit uns Ru:cksprache zu halten.

Die Station legt wert auf ein buntes Zusammenleben. Rassismus, Sexismus, Homophobie oder a:hnliche Diskriminierungen haben an Bord absolut nichts verloren.
Grundsa:tzlich ist der Zutritt zur Station nur vollja:hrigen Kohlenstoffeinheiten gestattet. Fu:r das CoderDojo macht die Station eine Ausnahme. Nach Beendigung des CoderDojos mu:ssen Minderja:hrige daher leider wieder von Bord gehen.
Falls Du Dich wunderst, warum wir die Umlaute so komisch schreiben... eigentlich weil die verwendete Schriftart nicht alle anbietet und wir aber gerade keine andere verwenden mo:chten. Und uneigentlich, weil es guter Einstieg ins [c_lang ist - die Sprache der Raumstation](https://wiki.c-base.org/dokuwiki/c-lang "c-lang").


