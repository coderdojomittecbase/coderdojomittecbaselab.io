---
title: Mentoren

---
# Mentoren

*Das wichtigste vorab:* Mentoren gesucht! Wenn Du gleich welche Sprache programmieren kannst oder weißt auf welcher Seite der Lo:tkolben heiss wird und Interesse an der Arbeit mit Kindern und Jugendlichen hast - melde Dich bei uns. 

{{< figure src="/images/derMicha.jpg" title="derMicha" >}}
Michael Merz (/derMicha) ist seit mehr als 20 Jahren als Software-Entwickler und -Architekt ta:tig. Er ist unter anderem Mitgru:nder der ubirch GmbH. Beruflich liegen seine Schwerpunkte bei Scala, Java, IoT und Big Data. In seiner Freizeit veranstaltet er für Kinder und Jugendliche Kurse
unter anderem beim Chaos Communicaton Congress mit Calliope und setzt Scratch ein. Ferner engagiert sich derMicha bei „Chaos macht Schule“ und vermittelt Kindern und Jugendlichen in Schulen das Thema Datenschutz. derMicha ist im Friedrichshainer Hacker- undMakerspace [xHain](https://x-hain.de/de/ "xHain") aktiv.

{{< figure src="/images/bengoshi.jpg" title="bengoshi" >}}
Kai Kobscha:tzki (/bengoshi) ist im zivilen Leben Rechtsanwalt. Er leitete mehr als sieben Jahre ehrenamtlich die Jugendarbeit in einer katholischen Gemeine im Prenzlauer Berg. Daneben begeistert er sich aber auch für Python, Django, PostgreSQL, Debian und Mailserver. Er ist seit über zehn Jahren aktives Mitglied im [c-base e.V.](https://c-base.org "c-base"), davon einige Jahre im Vorstand.


