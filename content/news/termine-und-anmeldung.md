+++
title = "Termine und Anmeldung"

+++
*Kurzfassung*:
Der na:chste Termine des CoderDojos ist:

* 24. Juni 2020 - 20 Uhr bis circa 21 Uhr virtuelles Treffen - wie geht es weiter?

Wir treffen uns hier: [https://bbb.cyber4edu.org/b/ben-caf-3dd](Big Blue Button). 

*Langfassung:*

Liebe Padawans,

wir hoffen, Ihr seid alle gut durch die Corona-Zeit gekommen. Pandemiebedingt ruhte leider unser CoderDojo. Wir hatten zwar intern mehrfach über ein virtuelles CoderDojo gesprochen, waren aber beide gut ausgelastet in dieser Zeit. Daru:ber wu:rden wir online gerne kurz berichten.

Kurz bevor die Sommerferien beginnen würden wir Euch zu einer spontanen Videokonferenz einladen: Morgen Abend, Mittwoch 24. Juni 2020 um 20 Uhr. Das ist zwar schon etwas spa:ter, aber Ihr habt ja Schulfrei. Vorher la:uft ab 16 Uhr [https://jugendhackt.org/live/](Jugend hackt). Wir wollen Euch natu:rlich die Mo:glichkeit geben, das live zu verfolgen.

Zuna:chst einmal interessiert uns natürlich, wie Ihr durch die Zeit gekommen seid. Habt Ihr Projekte weiterverfolgt, neue angefangen oder kam es irgendwie nicht dazu (wenn ja, woran hing 's? Wo können wir Euch unterstützen?)?

Gefu:hlt starten wir ein bisschen neu durch. Daher interessiert uns am meisten - wie soll es weitergehen? Wollt Ihr beispielsweise ein Programm in den Sommerferien? Welche Themenschwerpunkte sollen zuku:nftig gesetzt werden? Wollt Ihr beispielsweise Euch o:fter treffen und austauschen oder soll es Projekte für einen Vormittag geben oder soll es auch Reihen geben (wie beispielsweise der Start mit Python)?

Ob wir uns in oder nach den Sommerferien wieder in geschlossenen Ra:umen treffen ko:nnen, können wir nicht vorhersagen. Daher stellt sich für uns auch die Frage - soll es CoderDojo als Videokonferenz geben? Wa:re vielleicht unabha:ngig dessen ein „Mischbetrieb“ (alle zwei Wochen im Wechsel, einmal virtuell, ein real) sinnvoll?

Wir wu:rden uns sehr freuen, mit Euch für circa eine Stunde morgen Abend daru:ber sprechen zu ko:nnen! Wir treffen uns hier:

[https://bbb.cyber4edu.org/b/ben-caf-3dd](BigBlueButton)

Wenn Ihr keine Kamera habt - kein Ding. Man kann gut auch nur mit Ton teilnehmen. Auch mit dem Handy geht es - dann sogar teilweise mit Kamera.

Solltet Ihr noch Fragen haben - meldet Euch bitte einfach bei uns.


Beste Gru:Ce


derMicha und bengoshi


{{< figure src="/images/ATTiny85.jpg" title="ATTiny85" >}}

