---
title: Inhalte

---
# Inhalte

Der wichtigste Inhalt ist - das, was Du und die anderen gemeinsam machen wollen. Du arbeitest an einem coolen Projekt? Stell es uns vor! Du kommst nicht weiter? Lass Dir von anderen Jugendlichen oder Mentoren helfen.

Ansonsten bieten wir Einfu:hrungen und weitergehende Projekte mit dem Calliope und Scratch an. Dies ko:nnen kleine Spiele auf dem Calliope umfassen oder wir basteln mit RGB-LEDs, LED-Stripes oder Sensoren und Motoren an dem Calliope.

Wenn Du im Programmieren Schritte weitergehen willst, ist Python eine gute Wahl. Relativ schnell kann man kleine Spiele mit der Bibliothek pygame schreiben - aber auch gro:ssere Projekte, Web- oder Desktopanwendungen sind damit mo:glich.

Ferner schwingen wir auch ganz gerne einen Lo:tkolben und helfen Dir bei den Grundlagen im Bereich Elektrotechnik. Spannend sind beispielsweise Dinge die mit einem Mikrocontroller wie dem ATTiny85. Wir zeigen Dir, wie man eine Schaltung damit aufbaut, verlo:tet und programmiert. Dabei lernst Du gleich etwas C.

Klar sind Themen wie Linux, git oder Hackerethic immer auch ein Thema. Und ganz vorne an steht - viel Spaß am Gera:t. Wir machen keinen Unterricht, sondern wir wollen gemeinsam coole Dinge tun. Du hast Themen, die Du Dir wu:nschst? Sag und einfach rechtzeitig Bescheid. Du hast Probleme, bei denen Du stecken bleibst? Gib uns vorher einen Hinweis, damit wir gegebenenfalls schauen können, ob wir bei weiteren Mentoren um Unterstu:tzung anfragen ko:nnen.

