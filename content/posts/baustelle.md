+++
date = "2019-10-05T22:00:00+00:00"
title = "Baustelle"

+++
Okay, hier ist noch nix...

Warum ist hier nix?

Weil wir noch nix gemacht haben.

Aber Du bist herzlich eingeladen, beim nächsten CoderDojo@c-base am 27. Oktober 2019 von 10 bis 14 Uhr die Seite mit entstehen zu lassen.

    Melde Dich einfach hier an: https://zen.coderdojo.com/dojos/de/berlin/berlin-mitte-c-base