---
title: Teilnehmerinfos

---
# Teilnehmerinfos

Die Seite entsteht noch... was wir aber schon sagen ko:nnen: Das CoderDojo Mitte @c-base ist für junge Menschen von 12 bis 18 Jahren gedacht. Ju:ngere Teilnehmer du:rfen wir bitten, sich an das CoderDojo Friedrichshain zu wenden. A:lteren Teilnehmern helfen wir gerne weiter, wo sie gut andocken ko:nnen.
Du musst keine Vorkenntnisse haben. Interesse an Technik und / oder am Coden genu:gt vo:llig. Wichtig ist, dass Du Lust darauf hast, Dinge zu tun. Wir haben immer ein wenig Programm vorbereitet, sind aber offen für Deine Vorschla:ge, Wu:nsche und Ideen. Wenn Du die uns etwas vor dem CoderDojo schon mitteilst, ko:nnen wir schauen, ob wir zur Umsetzung Deiner Idee etwas vorbereiten mu:ssen.

Zum CoderDojo wa:re es super, wenn Du ein Notebook mitbringst. Wenn Du (noch) kein Notebook hast - kein Ding. Sag uns einfach vorher Bescheid und bekommst für die Zeit ein Leihgera:t von uns zur Verfu:gung gestellt. Wenn Du bereits mit einem Calliope, Arduino, Raspy oder was auch immer arbeitest - super, bring mit und zeig uns, was Du damit machst oder wo Du gerade feststeckst und lass Dir helfen.

Ach ja - wir treffen uns auf einer Raumstation. Da ist das Leben bunt. Uns ist es also vo:llig egal, ob du ma:nnlich, weiblich, sa:chlich oder was auch immer bist, ob Du von hier kommst oder nicht. Wir gehen davon aus, dass die Teilnehmer sich untereinander auch entsprechend respektieren. Be excellent to each other!
