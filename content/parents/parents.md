---
title: Infos für Eltern

---
# Infos für Eltern

Das CoderDojo richtet sich an Jugendliche im Alter von 12 bis 18 Jahren. Wir wollen mit den Kindern und Jugendlichen lo:ten und programmieren. Wir verfu:gen über Calliopes, ein paar Lo:tstationen und (wenigen) Notebooks. Unsere Arbeit erfolgt ehrenamtlich; die CoderDojos sind kostenfrei. Sofern Bausa:tze in Anspruch genommen werden, bitten wir um eine Spende in Ho:he der entsprechenden Unkosten.
Unsere Dojos enden gegen 14 Uhr. Nicht immer ist es mo:glich, pu:nktlich aufzuho:ren, wenn man gerade mitten im coden oder basteln ist. Bis 15 Uhr ist aber definitiv Feierabend und Minderja:hrige mu:ssen die Station wieder verlassen. Auf der Station gilt (leider) die Regel, dass Minderja:hrige nicht an Bord du:rfen. Unsere Ausnahme hierfu:r endet dann. Mitglieder der Station (Mindestalter 18 Jahre) du:rfen unter den u:blichen Voraussetzungen natu:rlich immer an Bord sein.
Da wir u:ber die Mittagszeit kommen, bestellen wir in der Regel gemeinsam Pizza. Wir wu:rden Dich bitten, Deinem Kind hierfu:r etwas Geld mitzugeben. Natu:rlich kann man sich auch etwas zu essen mitbringen. Teller und Besteck sind vorhanden. Leider verfu:gen wir u:ber keine Mo:glichkeit, etwas zu erwa:rmen. Getra:nke ko:nnen mitbetracht werden. Es besteht aber auch die Mo:glichkeit für zwei Euro Softdrinks zu erwerben.
Wir veranstalten unsere CoderDojos in der c-base. Hierbei handelt es sich um einen Verein, der Miete, Strom und a:hnliches aus den Mitgliederbeitra:gen und Spenden bestreitet. Daher ist die Station natürlich immer für Spenden beziehungsweise Mitgliedsbeitra:ge dankbar.
